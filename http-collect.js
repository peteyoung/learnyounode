var http = require('http');

var url = process.argv[2];

var data = "";

http.get(url, function(res) {
	res.setEncoding('utf8');
	res.on('error', function(err) {
		console.log("res error: "+err);
	}).on('data', function(chunk) {
		data += chunk;
	}).on('end', function() {
		console.log(data.length);
		console.log(data);
	});
}).on('error', function(err) {
	console.log("get error: "+err);
});


