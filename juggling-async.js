var http = require('http');

var urls = [];
var data = [];

for (i=0; i<3; i++) {
    urls[i] = process.argv[i+2];
    data[i] = "";
}

var call_count = 0;

function get_data(idx) {
    call_count++;
    //console.log("calling  "+call_count+" "+urls[idx]);
    http.get(urls[idx], function(res) {
        res.setEncoding('utf8');
        res.on('error', function(err) {
            console.log("res error: "+err);
            call_count--;
        }).on('data', function(chunk) {
            data[idx] += chunk;
        }).on('end', function() {
            call_count--;
            if (call_count == 0) {
                for (i=0; i<3; i++) {
                    console.log(data[i]);
                }
            }
        });
    }).on('error', function(err) {
        console.log("get error: "+err);
        call_count--;
    });
}

for (i=0; i<3; i++) {
    get_data(i);
}
