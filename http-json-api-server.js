var http = require('http');
var url  = require('url');

var port = process.argv[2];

http.createServer(function (req, res) {
    var urlnfo = url.parse(req.url, true);
    var fmturl = url.format(urlnfo);

    if (urlnfo.pathname == '/api/parsetime') {
        respond_with_time(get_hms(urlnfo.query["iso"]), res);
    }
    else if (urlnfo.pathname == '/api/unixtime') {
        respond_with_time(get_unix(urlnfo.query["iso"]), res);
    }
    else {
        response.writeHead(500, { 'Content-Type': 'text/plain' });
        response.end('Failsauce');
    }

}).listen(port);

function respond_with_time(time, response) {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end(JSON.stringify(time));
}

function get_hms(iso_time) {
    var date = new Date(iso_time);
    return {
        "hour":   date.getHours(),
        "minute": date.getMinutes(),
        "second": date.getSeconds()
    }
}

function get_unix(iso_time) {
    var date = new Date(iso_time);
    return { "unixtime": date.getTime() }
}

/*
node -pe "require('url').parse('http://localhost:22021/api/parsetime?iso=2013-08-10T12:10:15.474Z', true)"

{ protocol: 'http:',
  slashes: true,
  auth: null,
  host: 'localhost:22022',
  port: '22022',
  hostname: 'localhost',
  hash: null,
  search: '?iso=2013-08-10T12:10:15.474Z',
  query: { iso: '2013-08-10T12:10:15.474Z' },
  pathname: '/api/parsetime',
  path: '/api/parsetime?iso=2013-08-10T12:10:15.474Z',
  href: 'http://localhost:22022/api/parsetime?iso=2013-08-10T12:10:15.474Z' }
*/
