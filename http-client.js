var http = require('http');

var url = process.argv[2];

http.get(url, function(res) {
	//console.log("response: "+res.statusCode);
	res.setEncoding('utf8');
	res.on('error', function(err) {
		console.log("res error: "+err);
	}).on('data', function(chunk) {
		console.log(chunk);
	});
}).on('error', function(err) {
	console.log("get error: "+err);
});

