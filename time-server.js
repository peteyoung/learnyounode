var net = require('net');

// npm install strftime
var strftime = require('strftime');

var port = process.argv[2];

net.createServer(function (socket) {
    // "2013-07-06 07:42"
    socket.end(strftime("%F %H:%M")+"\n");
}).listen(port);
