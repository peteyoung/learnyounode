learnyounode challenge solutions
===
Solutions to learnyounode challenges

Some examples use the [underscore library for nodejs][1].
`npm install underscore-node`

Recommended modules used
---
bl
`npm install bl`

strftime
`npm install strftime`

[1]: https://www.npmjs.org/package/underscore-node
