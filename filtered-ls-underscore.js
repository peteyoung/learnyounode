var fs = require('fs');
var path = require('path');
var _ = require('underscore-node');

var path_arg = process.argv[2];
var file_ext = process.argv[3];

fs.readdir(path, function(err, files) {
    if (err) {
        console.log(err);
    } else {
        var filtered = _.filter(files, function(file) {
            path.extname(file) == file_ext;
        });
        _.map(filtered, function (file) {
            console.log(file);
        }
    }
};
