var fs = require('fs');

var fileName = process.argv[2];

fs.readFile(fileName, 'utf8', function(err, text) {
	if (err) {
		console.log(err);
		return;
	}

	var ary  = text.split('\n');
	var count = ary.length - 1;
	console.log(count);
});
