var fs   = require('fs'),
	path = require('path');

var dirName = process.argv[2];
var extName = process.argv[3];
//console.log('ext: '+extName);
fs.readdir(dirName, function(err, list) {
	if (err) {
		console.log(err);
		return;
	}

	for (i=0; i<list.length; i++) {
		//console.log(path.extname(list[i])+' == '+extName);
		if (path.extname(list[i]) == '.'+extName) {
			console.log(list[i]);
		}
	}
});
