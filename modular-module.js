var fs   = require('fs');
	path = require('path');

module.exports = function(dirName, extName, callback) {
	fs.readdir(dirName, function(err, list) {
		if (err) {
			return callback(err);
		}

		var ary = [];
		for (i=0; i<list.length; i++) {
			if (path.extname(list[i]) == '.'+extName) {
				ary.push(list[i]);
			}
		}

		callback(null, ary);
	});
}


